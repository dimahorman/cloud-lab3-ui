import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FarmField} from './FarmField';
import {Sensor} from '../tab-content-sensor/Sensor';
import {FarmService} from '../service/farm.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-tab-content-field',
  templateUrl: './tab-content-field.component.html',
  styleUrls: ['./tab-content-field.component.scss']
})
export class TabContentFieldComponent implements OnInit, OnDestroy {
  private sensorsSubscription: Subscription;
  @Input() field: FarmField;
  sensors: Array<Sensor>;

  constructor(private farmService: FarmService) {
    this.sensors = new Array<Sensor>();
  }

  ngOnInit(): void {
    this.fetchSensors();
  }

  ngOnDestroy(): void {
    this.sensorsSubscription.unsubscribe();
  }

  private fetchSensors(): void {
    this.sensorsSubscription = this.farmService.getSensorsByFieldId(this.field.id).subscribe(resp => {
      resp.body.forEach(elem => {
        this.sensors.push(elem);
      });
    });
  }
}
