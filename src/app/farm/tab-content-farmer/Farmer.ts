export interface Farmer {
  name: string;
  age: number;
  birthCity: string;
  skillLevel: SkillLevel;

}

export const enum SkillLevel {
  HIGH= 'high',
  AVERAGE = 'average',
  LOW = 'low'
}
