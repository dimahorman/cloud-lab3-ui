import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Farmer} from './Farmer';
import {Farm} from '../tab-content-farm/farm';
import {Subscription} from 'rxjs';
import {FarmService} from '../service/farm.service';

@Component({
  selector: 'app-tab-content-farmer',
  templateUrl: './tab-content-farmer.component.html',
  styleUrls: ['./tab-content-farmer.component.scss']
})
export class TabContentFarmerComponent implements OnInit, OnDestroy {
  private farmSubscription: Subscription;
  farms: Array<Farm>;
  @Input() farmer: Farmer;

  constructor(private farmService: FarmService) {
    this.farms = new Array<Farm>();
  }

  ngOnInit(): void {
    this.fetchFarms();
  }

  ngOnDestroy(): void {
    this.farmSubscription.unsubscribe();
  }

  private fetchFarms(): void {
    this.farmSubscription = this.farmService.getFarmsByFarmerName(this.farmer.name).subscribe(resp => {
      resp.body.forEach(elem => {
        this.farms.push(elem);
      });
    });
  }
}
