import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContentFarmComponent } from './tab-content-farm.component';

describe('TabContentFarmComponent', () => {
  let component: TabContentFarmComponent;
  let fixture: ComponentFixture<TabContentFarmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabContentFarmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContentFarmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
