import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Farm} from './farm';
import {FarmField, FarmFieldType} from '../tab-content-field/FarmField';
import {FarmService} from '../service/farm.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-tab-content-farm',
  templateUrl: './tab-content-farm.component.html',
  styleUrls: ['./tab-content-farm.component.scss']
})
export class TabContentFarmComponent implements OnInit, OnDestroy {
  private farmFieldsSubscription: Subscription;
  @Input() farm: Farm;
  farmFields: Array<FarmField>;

  constructor(private farmService: FarmService) {
    this.farmFields = new Array<FarmField>();
  }

  ngOnInit(): void {
    this.fetchFarmFields();
  }

  ngOnDestroy(): void {
    this.farmFieldsSubscription.unsubscribe();
  }

  private fetchFarmFields(): void {
    this.farmFieldsSubscription = this.farmService.getFarmFieldsByFarmId(this.farm.id).subscribe(resp => {
      resp.body.forEach(elem => {
        this.farmFields.push(elem);
      });
    });
  }
}
