import {Component, OnDestroy, OnInit} from '@angular/core';
import {Farmer} from './tab-content-farmer/Farmer';
import {Subscription} from 'rxjs';
import {FarmService} from './service/farm.service';

@Component({
  selector: 'app-farm',
  templateUrl: './farm.component.html',
  styleUrls: ['./farm.component.scss']
})
export class FarmComponent implements OnInit, OnDestroy {
  private farmerSubscription: Subscription;
  farmers: Array<Farmer>;

  constructor(private farmService: FarmService) {
    this.farmers = new Array<Farmer>();
  }

  ngOnInit(): void {
    this.fetchFarmers();
  }

  ngOnDestroy(): void {
    this.farmerSubscription.unsubscribe();
  }

  private fetchFarmers(): void {
    this.farmerSubscription = this.farmService.getFarmers().subscribe(resp => {
      resp.body.forEach(elem => {
        this.farmers.push(elem);
      });
    });
  }
}
