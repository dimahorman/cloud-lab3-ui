import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Sensor} from './Sensor';

@Component({
  selector: 'app-tab-content-sensor',
  templateUrl: './tab-content-sensor.component.html',
  styleUrls: ['./tab-content-sensor.component.scss']
})
export class TabContentSensorComponent implements OnInit, OnDestroy {
  @Input() sensor: Sensor;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }
}
