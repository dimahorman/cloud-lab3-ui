import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContentSensorComponent } from './tab-content-sensor.component';

describe('TabContentSensorComponent', () => {
  let component: TabContentSensorComponent;
  let fixture: ComponentFixture<TabContentSensorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabContentSensorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContentSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
