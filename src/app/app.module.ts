import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FarmComponent } from './farm/farm.component';
import { TabContentFarmerComponent } from './farm/tab-content-farmer/tab-content-farmer.component';
import { TabContentFarmComponent } from './farm/tab-content-farm/tab-content-farm.component';
import { TabContentFieldComponent } from './farm/tab-content-field/tab-content-field.component';
import { TabContentSensorComponent } from './farm/tab-content-sensor/tab-content-sensor.component';
import {MatTabsModule} from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    FarmComponent,
    TabContentFarmerComponent,
    TabContentFarmComponent,
    TabContentFieldComponent,
    TabContentSensorComponent,
  ],
  imports: [
    BrowserModule,
    MatTabsModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
